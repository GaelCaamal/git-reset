<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeUseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
   function test_welcome_nickname(){
    $responce = $this->get('saludo/gael/zero');
    $responce->assertStatus(200);
    $responce->assertSee("Bienvenido gael: tu clave es zero");

   }
   function test_welcome_no_nickname(){
    $responce = $this->get('saludo/gael');
    $responce->assertStatus(200);
    $responce->assertSee("Bienvenido gael, no tienes apodo");
   }
}
