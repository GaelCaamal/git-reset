<?php
class Loteria{
    //atributos
    public $numero;
    public $intentos;
    public $resultado = false;
    
    //metodos
    public function __construct($numero, $intentos){
        $this->numero = $numero;
        $this->intentos = $intentos;
    }
    public function sortear(){
        $minimo = $this->numero / 2;
        $maximo = $this->numero *2;
        $int = rand($minimo, $maximo);
        for($i = 0; $i < $this->intentos; $i++){
            self::intentos($int);
        }
    }
    public function intentos($int){
        if($int == $this->numero){
            echo "<b>" . $int . "==" . $this->numero ." </b><br>";
            $this->resultado = true;
        }else{
            echo $int ."!=" . $this->numero . "<br>";
        }

    }
    public function __destructor(){
        if($this->resultado){
            echo "felilcidades ganaste " . $this->intentos . " intentos.";
        }else{
            echo "has perdido " . $this->intentos . " intentos.";
        }
    }
}
$loteria = new Loteria(10,10);
$loteria->sortear();