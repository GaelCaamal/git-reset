<?php

class Facebook{
    public $nombre;
    public $edad;
    private $pass;

    public function __construct($nombre, $edad, $pass){
        $this->nombre =$nombre;
        $this->edad = $edad;
        $this->pass = $pass;
    }
    public function verInformacion(){
        echo "Nombre: " . $this->nombre . "<br>";
        echo "Edad: " . $this->edad . "<br>";
        echo "password: " . $this->pass . " ";
    }
}
$facebook = new Facebook("Gael ", 20, "1234");
$facebook->verInformacion();