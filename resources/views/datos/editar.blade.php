<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

 <script src="https://kit.fontawesome.com/6074ba66fc.js"></script> 
    
     <link rel="stylesheet" href=" {{asset('bootstrap/css/bootstrap.css')}} ">
  <script src=" {{asset('bootstrap/jquery/jquery-3.4.1.js')}} " ></script>
  <script src=" {{asset('js/popper.min.js')}} " ></script>
  <script src=" {{asset('bootstrap/js/bootstrap.js')}} " ></script>

</head>
<body>
    
   
    
    <div class="container col-5 m.auto">
        <div class="form-row container ">
            <div class="row mr-4">
                <h1>Editar Usuario Con ID Num {{$dato->id}} </h1>
            </div>

            <div class="row m-auto">
                <a href="/">
                    <button type="button" class="btn btn-primary">Regresar A La Tabla</button>
                </a>
            </div>
        </div>

             @if (session('mensaje'))
    
                <div class="alert alert-success"> {{ session('mensaje') }} </div>
    
             @endif
        
         <form action=" {{route('datos.update',$dato->id)}} " method="POST">
            @method('put')

        @csrf
        <div>
          <label for="validationDefault01">Nombre</label>
          <input type="text" name="nombre" 
          id="validationDefault01" placeholder="Nombre" 
          class="form-control mb-2"   required
            value=" {{$dato->nombre}} " >
        </div>

        <div>
          <label for="validationDefault01">Descripcion</label>
          <input type="text" name="descripcion" 
          id="validationDefault02" placeholder="Descripcion" 
          class="form-control mb-2" required
            value=" {{$dato->descripcion}} " >
          
        </div>
        <button type="submit" class="btn btn-primary btn-block">Guardar Cambios</button>
      </form>
      </div>
</body>
</html>

