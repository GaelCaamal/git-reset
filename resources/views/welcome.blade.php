<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6074ba66fc.js"></script> 
    
     <link rel="stylesheet" href=" {{asset('bootstrap/css/bootstrap.css')}} ">
  <script src=" {{asset('bootstrap/jquery/jquery-3.4.1.js')}} " ></script>
  <script src=" {{asset('js/popper.min.js')}} " ></script>
  <script src=" {{asset('bootstrap/js/bootstrap.js')}} " ></script>

    <title>crud con laravel</title>
  </head>
  <body>

    
   



<div class="container col-8 m-auto ">

  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    <i class="fas fa-user-plus">Nuevo</i>
  </button>
   @if (session('mensaje'))
    <div class="alert alert-success">
        {{session('mensaje')}}
    </div>
        
    @endif
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">agregar usuario nuevo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        
         <form action=" {{route('datos.crear')}} " method="POST">

        @csrf
        <div>
          <label for="validationDefault01">Nombre</label>
          <input type="text" name="nombre" id="validationDefault01" placeholder="Nombre" class="form-control mb-2"   required >
        </div>

        <div>
          <label for="validationDefault01">Descripcion</label>
          <input type="text" name="descripcion" id="validationDefault02" placeholder="Descripcion" class="form-control mb-2" required >
          
        </div>
        <button type="submit" class="btn btn-primary btn-block">Agregar</button>
      </form>
      </div>
      
      </div>
    </div>
  </div>
</div>
     

    <table class="table table-dark col-8 m-auto mt-2">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Error</th>
      <th scope="col">DEPENDENCIA</th>
      <th scope="col">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($datos as $item)        
    <tr>
      <th scope="row"> {{$item->id}} </th>
      <td> {{$item->nombre}} </td>
      <td> {{$item->descripcion}} </td>
      <td>

        <a href=" {{route('datos.editar',$item)}} " class="btn btn-success btn-sm mr-3">
          <i class="fas fa-edit ">Editar</i>
        </a>
        
        
          <form action=" {{route('datos.eliminar', $item)}} " method="POST" class="d-inline" >
            @method('DELETE')
            @csrf
            <button class="btn btn-danger btn-sm" type="submit"  >
              <i class="fas fa-trash-alt "  >Eliminar</i>
            </button>
          </form>
          
      </td>
    </tr>
    @endforeach
    
    
  </tbody>
</table>












    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>