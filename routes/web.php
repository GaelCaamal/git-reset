<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/usuarios','UserController@index');

 Route::get('/usuarios/{id}', 'UserController@show')
 ->where('id', '[0-9]+');


Route::get('/usuarios/nuevo','UserController@create');

Route::get('/saludo/{name}/{nickname?}', 'WelcomeUserController@index');

Route::post('/usuarios/crear', 'UserController@store');

Route::get('/php', function (){
    return view('practica');
});

Route::get('/php1',function(){
    return view('practica1');
});

Route::get('/php2',function(){
    return view('practica2');
});

Route::get('/php3',function(){
    return view('practica3');
});

Route::get('/4/herencia',function(){
    return view('herencia');
});


Route::get('/','webcontrolador@inicio');

Route::post('/','webcontrolador@crear')->name('datos.crear');

Route::get('/editar/{id}', 'webcontrolador@datos')->name('datos.editar');

Route::put('/editar/{id}', 'webcontrolador@update')->name('datos.update');

Route::delete('eliminar/{id}', 'webcontrolador@eliminar')->name('datos.eliminar');



