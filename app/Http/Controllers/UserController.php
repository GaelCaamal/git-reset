<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
  
   public function index(){
       if (request()->has('empty')){
           $users = [];
       }else{
         $users =[
            'zero',
            'joe',
            'kallen',
            'zuzaku',
            'auphemia',
        ];
       }
       
       

       return view('users',[
           'users'=>$users,
           'title' => 'listado de usuarios'
       ]);
   }
   public function show($id){
       return "detalles de usuario:{$id}";
   }
   public function create(){
       return 'creando nuevo usuarios:';
   }
}
