<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class webcontrolador extends Controller
{
    public function inicio(){
        $datos = App\Dato::all();
        return view('welcome',compact('datos'));
    }

    public function crear(Request $request){
        //return $request->all();

        $datoNuevo =new App\Dato;
        $datoNuevo->nombre = $request->nombre;
        $datoNuevo->descripcion = $request->descripcion;

        $datoNuevo->save();

        return back()->with('mensaje', 'Usuario Nuevo Creado');
    }

    public function datos($id){
        $dato = App\Dato::findOrFail($id);
        return view('datos.editar', compact('dato'));
    }

    public function update(Request $request, $id){
        $datoUpdate = App\Dato::findOrFail($id);
        $datoUpdate->nombre = $request->nombre;
        $datoUpdate->descripcion = $request->descripcion;

        $datoUpdate->save();
        return back()->with('mensaje', 'datos actualizados');
    }

    public function eliminar($id){
        $datoEliminar = App\Dato::findOrFail($id);
        $datoEliminar->delete();

        return back()->with('mensaje', 'Usuario Eliminado');
    }
}

